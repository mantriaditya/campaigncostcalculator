package com.channels.service;

public interface IChannel {
	
	/**
	 * 
	 * @param audienceSize
	 * @return
	 */
	public double calculate(long audienceSize);

}
