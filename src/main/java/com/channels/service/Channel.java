package com.channels.service;

import java.util.List;

public abstract class Channel implements IChannel {

	/**
	 * This method calculates the campaign cost
	 * 
	 * @param audienceSize       denotes the size of audience
	 * @param base_campaign_fee  is the base campaign fee for channel
	 * @param audienceSizeList   gets the threshold bracket at each level
	 * @param transactionFeeList gets the transaction fee for each threshold level
	 * @return the total calculated cost for this campaign
	 */
	public double calculateCampaignCost(long audienceSize, double base_campaign_fee, List<Integer> audienceSizeList,
			List<Double> transactionFeeList) {

		double campaignCost = 0L;
		campaignCost += base_campaign_fee;
		int index = 0;
		while (audienceSize > 0) {
			if (index != transactionFeeList.size() - 1) {
				if (audienceSize >= audienceSizeList.get(index)) {
					campaignCost += (audienceSizeList.get(index) * transactionFeeList.get(index));
					audienceSize -= audienceSizeList.get(index);
				} else {
					campaignCost += audienceSize * transactionFeeList.get(index);
					return campaignCost;
				}
			} else {
				campaignCost += transactionFeeList.get(index) * audienceSize;
			}
			index++;

		}
		return campaignCost;
	}
}