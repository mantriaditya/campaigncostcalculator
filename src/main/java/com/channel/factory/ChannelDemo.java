package com.channel.factory;

import com.channel.configuration.ChannelConfigurator;
import com.channel.implementation.DirectMailChannel;
import com.channel.implementation.EmailChannel;
import com.channel.implementation.SMSChannel;
import com.channels.service.Channel;

public class ChannelDemo {

	private static Channel channelService;
	private static ChannelConfigurator channelConfigurator;

	public static void main(String[] args) throws Exception {
		if (args.length < 4) {
			throw new Exception("Invalid number of arguments. The expected format is -size 1009 -channels EM|SM");
		}
		long size = 0;
		String channels[] = null;
		try {
			size = Integer.parseInt(args[1]);
			channels = args[3].split("\\|");

		}

		catch (Exception e) {
			System.out.println("Exception occured. Please verify input argument format. Expected format is -size 1009 -channels EM|SM \n");
			e.printStackTrace();
		}

		calculateCampaignCost(size, channels);
	}

	/**
	 * Calculates the total campaign cost
	 * 
	 * @param size
	 * @param channels
	 */
	private static void calculateCampaignCost(long size, String[] channels) {
		double campaignCost = 0;
		for (String channel : channels) {
			channelService = getChannelService(channel);
			if (channelService != null) {
				campaignCost += channelService.calculate(size);
			}
		}
		System.out.println("Campaign total cost is: " + campaignCost);
	}

	/**
	 * Create specific channel implementation class
	 * 
	 * @param channel
	 * @return
	 */
	private static Channel getChannelService(String channel) {
		// get Singleton instance of the Channel Configuration
		channelConfigurator = ChannelConfigurator.getInstance();
		if (channel.equals("EM")) {
			return new EmailChannel(channelConfigurator.getAudienceConfguration(),
					channelConfigurator.getTransactionFeeConfiguration(channel));
		} else if (channel.equals("SMS")) {
			return new SMSChannel(channelConfigurator.getAudienceConfguration(),
					channelConfigurator.getTransactionFeeConfiguration(channel));
		} else if (channel.equals("DM")) {
			return new DirectMailChannel(channelConfigurator.getAudienceConfguration(),
					channelConfigurator.getTransactionFeeConfiguration(channel));
		}
		return null;
	}

}
