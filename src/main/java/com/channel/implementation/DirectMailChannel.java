package com.channel.implementation;

import java.util.List;

import com.channel.constants.ChannelConstants;
import com.channels.service.Channel;

public class DirectMailChannel extends Channel {

	private double base_campaign_fee;
	List<Integer> audienceSizeList;
	List<Double> transactionFeeList;

	/**
	 * Constructor method to initialize the object
	 * @param audienceSizeList
	 * @param transactionFeeList
	 */
	public DirectMailChannel(List<Integer> audienceSizeList, List<Double> transactionFeeList) {

		base_campaign_fee = ChannelConstants.DIRECT_EMAIL_BASE_CAMPAIGN_FEE;
		this.audienceSizeList = audienceSizeList;
		this.transactionFeeList = transactionFeeList;
	}

	public double calculate(long audienceSize) {
		return super.calculateCampaignCost(audienceSize, this.base_campaign_fee, this.audienceSizeList,
				this.transactionFeeList);
	}

}
