package com.channel.constants;

public class ChannelConstants {

	public static final double EMAIL_BASE_CAMPAIGN_FEE = 1500;
	public static final double SMS_BASE_CAMPAIGN_FEE = 1800;
	public static final double DIRECT_EMAIL_BASE_CAMPAIGN_FEE = 8000;
	
}
