package com.channel.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ChannelConfigurator {

	Map<String, List<Double>> transactionFeeConfigurationMap;
	List<Double> tranasctionFeeEmailList;
	List<Double> tranasctionFeeSMSList;
	List<Double> tranasctionFeeDirectMailList;
	List<Integer> audienceSizeList;

	private static ChannelConfigurator instance;
	public String value;
	
	private ChannelConfigurator() {
		tranasctionFeeEmailList = new ArrayList<Double>();
		tranasctionFeeSMSList = new ArrayList<Double>();
		tranasctionFeeDirectMailList = new ArrayList<Double>();
		audienceSizeList = new ArrayList<Integer>();

		transactionFeeConfigurationMap = new HashMap<String, List<Double>>();

		// Email Cost bands
		tranasctionFeeEmailList.add(0.0);
		tranasctionFeeEmailList.add(0.66);
		tranasctionFeeEmailList.add(0.42);
		tranasctionFeeEmailList.add(0.27);
		tranasctionFeeEmailList.add(0.17);

		// SMS Cost bands
		tranasctionFeeSMSList.add(0.0);
		tranasctionFeeSMSList.add(1.10);
		tranasctionFeeSMSList.add(0.57);
		tranasctionFeeSMSList.add(0.41);
		tranasctionFeeSMSList.add(0.25);

		// DirectMail Cost bands
		tranasctionFeeDirectMailList.add(0.0);
		tranasctionFeeDirectMailList.add(1.56);
		tranasctionFeeDirectMailList.add(1.00);
		tranasctionFeeDirectMailList.add(0.70);
		tranasctionFeeDirectMailList.add(0.62);

		// Fee Configuration Map
		transactionFeeConfigurationMap.put("EM", tranasctionFeeEmailList);
		transactionFeeConfigurationMap.put("SMS", tranasctionFeeSMSList);
		transactionFeeConfigurationMap.put("DM", tranasctionFeeDirectMailList);

		// Bands of audience Size
		audienceSizeList.add(5000);
		audienceSizeList.add(10000);
		audienceSizeList.add(10000);
		audienceSizeList.add(25000);

	}

	/**
	 * Method to create single instance of this class
	 * @return 
	 */
	public static ChannelConfigurator getInstance() {
		if (instance == null) {
			instance = new ChannelConfigurator();
		}
		return instance;
	}

	public List<Integer> getAudienceConfguration() {
		return audienceSizeList;
	}

	public List<Double> getTransactionFeeConfiguration(String channel) {
		return transactionFeeConfigurationMap.get(channel);
	}

}
