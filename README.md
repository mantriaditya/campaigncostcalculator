# Campaign Cost Calculator

## Steps to run the application:
1. run the cmd in cmd-prompt `mvn clean install`
2. This will create a jar file located at **/CampaignCostCalculator/target/CampaignCostCalculator-0.0.1-SNAPSHOT.jar**
3. Run the jar file using command `java -cp target/CampaignCostCalculator-0.0.1-SNAPSHOT.jar com.channel.factory.ChannelDemo -size 22000 -channels "EM|SMS"`
Note: In the above command 22000 is the size, and EM SMS are channels. Feel free to change the values and test.

## Steps to change the cost parameters

1. **/CampaignCostCalculator/src/main/java/com/channel/constants/ChannelConstants.java** holds the constants for Base Campaign Fee.
2. **/CampaignCostCalculator/src/main/java/com/channel/configuration/ChannelConfigurator.java** have the configuration values for bands(Thresholds) and the channel fees.
3. To create updated jar, Save the files and repeat above steps to run the application.  

## Code Structure

In order to keep the configuration as a single source, I have used Singleton Pattern to get single instance of ChannelConfigurator.java 

In order to create different channels of same type, I have used Factory Pattern.

For easy updates and Maintainability of code I tried to keep the logic for calculating cost in Abstract class **/CampaignCostCalculator/src/main/java/com/channels/service/Channel.java** 
